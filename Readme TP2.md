# Les base de Terraform

## Introduction

Le fichier présente les differentes commandes et les differentes étapes pour la mise en place de Terraform

## Config Terraform

### Les options de la commande Terraform

Pour avoir la liste des options de la commande Terraform, il suffit de lancer `terraform` dans le terminal.

### Création du fichier `instance.tf`

Pour la création du fichier `instance.tf`, il faut lancer la commande suivante:

```bash
touch instance.tf
```

Voici la descpription du contenu  du fichier `instance.tf`.

### Contenu du fichier `instance.tf`

Le fichier `instance.tf` contiendra la configuration nécessaire pour créer une instance Google Compute Engine avec les spécifications suivantes :

- Nom de projet : `NOM PROJET`
- Nom de l'instance : `terraform`
- Type de machine : `e2-medium`
- Zone : `europe-west1-d`
- Disque d'amorçage : utilisant l'image `debian-cloud/debian-11`
- Interface réseau : utilisant le réseau par défaut avec une configuration d'accès par défaut

```hcl
provider "google" {
  project = "NOM PROJET"
  region  = "europe-west1"
}

resource "google_compute_instance" "terraform_instance" {
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "europe-west1-d"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}
```

N'oubliez pas de remplacer `<PROJECT_ID>` par l'ID de votre projet.

### Étapes d'exécution Terraform

1. **Initialisation** : Téléchargez et installez le binaire du fournisseur Google Cloud Platform en exécutant la commande suivante :
   
   ```bash
   terraform init
   ```

2. **Plan d'exécution** : Il faut générer un plan détaillant les actions que Terraform prendra pour atteindre l'état désiré défini dans vos fichiers de configuration en exécutant :
   
   ```bash
   terraform plan
   ```

3. **Appliquer les changements** : Il faut appliquer les changements dans les fichiers de configuration avec la commande suivante:
   
   ```bash
   terraform apply
   ```

4. **Inspection de l'état actuel** : la commande suivante permet d'afficher les details des ressources créées et gérées par Terraform avec leurs attribitus et 
Pour afficher les détails des ressources créées et gérées par Terraform, y compris leurs attributs et configurations actuels, utilisez la commande :
   
   ```bash
   terraform show
   ```

## Conclusion

Une fois que vous avez suivi ces étapes, vous aurez déployé avec succès une instance Google Compute Engine en utilisant Terraform sur Google Cloud Platform.
