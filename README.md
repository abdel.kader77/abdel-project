# abdel Project

# Bases de la mise en réseau

Le fichier présente les différentes commandes et les differentes étapes pour gérer les réseaux personnalisés dans GCP.

## Authentification et Configuration

- `gcloud auth list`: Permet d'afficher les comptes Google Cloud qui sont authentifiés sur le moment sur la machine.
- `gcloud config list project`: Permet d'afficher l'identifiant du projet.

## Création d'un Réseau Personnalisé

1. **Création d'un Réseau Personnalisé**: tappez cette commande  dans le Cloud Shell :
   ```bash
   gcloud compute networks create taw-custom-network --subnet-mode custom
   ```

2. **Création d'un Sous-Réseau dans "Europe-West1"**:
   ```bash
   gcloud compute networks subnets create subnet-europe-west1 
   --network taw-custom-network 
   --region europe-west1 
   --range 10.0.0.0/16
   ```

   Cette commande permet de créer un sous-réseau dans "taw-custom-network" qui est dans europe-west1 avec l'étendue d'adresses IP spécifiée.

3. **Création de differents Sous-Réseaux**:
   Utilisez les meme commande pour des sous-réseaux dans d'autres régions avec des plages d'adresses IP spécifiques.

4. **Afficher la liste des réseaux**
   ```bash
   gcloud compute networks subnets list \
   --network taw-custom-network
  ```
## Gestion des Règles de Pare-feu

- **Création d'une règle qui permet d'autoriser le trafic HTTP**:
  ```bash
  gcloud compute firewall-rules create nw101-allow-http 
  --allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 
  --target-tags http
  ```

  Cela permet de créer une règle de pare-feu qui autorise le trafic TCP sur le port 80 (HTTP) depuis n'importe quelle adresse IP sur le réseau personnalisé "taw-custom-network".

- **Création d'autres règles de pare-feu**:
  Differentes regles peuvent etre créer pour autoriser différents types de trafic (ICMP, SSH, RDP)

## Création d'Instances

- **Création d'une instance dans la zone Europe-West1-D**:
  ```bash
  gcloud compute instances create us-test-01 
  --subnet subnet-europe-west1 
  --zone europe-west1-d 
  --machine-type e2-standard-2 
  --tags ssh,http,rules
  ```

  Ceci permet de créer une instance avec les spécifications fournies donc le sous-réseau, le type de machine, la zone, les tags associés.

- **Création de differentes instances dans différentes zones**:
  Utilisez des commandes similaires pour créer des instances dans différentes zones avec des sous-réseaux spécifiques et d'autres configurations.

## Vérification et Test

- **Vérification de la Connectivité**:
  Une fois que les instances et les règles de pare-feu sont mit en place, il faut tester la connectivité ( faire des `ping` -> IP externes des instances)

---

Ce guide fournit les bases pour la création et la gestion des réseaux et des instances dans Google Cloud Platform. Pour des informations plus détaillées, consultez la documentation officielle de GCP.

